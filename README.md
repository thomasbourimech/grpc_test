# grpc_test

    Simple grpc python squareroot test.
## Install:

    pip3 install grpcio-tools

## To generate **pb2** files:

    python3 -m grpc_tools.protoc -I ./protos --python_out=. --grpc_python_out=. protos/test.proto

## Start server:

    python3 test_server.py

## Run client:

    python3 test_client.py

