import grpc
import test_pb2
import test_pb2_grpc
import math
from concurrent import futures


class SquareRootServiceServicer(test_pb2_grpc.SquareRootServiceServicer):
    def squareRoot(self, request, context):
        result = math.sqrt(request.input)
        return test_pb2.Result(result = result)


def main():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=2))
    test_pb2_grpc.add_SquareRootServiceServicer_to_server(SquareRootServiceServicer(), server)
    print("Server starting...")
    server.add_insecure_port('[::]:50052')
    server.start()
    server.wait_for_termination()

main()